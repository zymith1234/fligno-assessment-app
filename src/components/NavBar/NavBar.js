import React, {Component} from 'react';
import {MenuItemsGuest} from './MenuItems'
import {MenuItemsUser} from './MenuItemsUser'
import {Button} from './Button'
import "./NavBar.css"

class Navbar extends Component {
  state = { clicked: false }

  handleClick = () => {
    this.setState({ clicked: !this.state.clicked })
  }
  
  render(){
    let user = sessionStorage.getItem("userData")
    console.log(user)
    return (
      <nav className="NavbarItems">
        <h1 className="navbar-logo">React<i className="fab fa-react"></i></h1>
        <div className="menu-icon" onClick={this.handleClick}> 
          <i className={this.state.clicked ? 'fa fa-times' : 'fa fa-bars'} aria-hidden="true"></i>
        </div>
        {
          user != null
          ?
          (
            <>
              <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                {MenuItemsUser.map((item, index) => {
                  return (
                    <li key={index}>
                      <a className={item.cName} href={item.url}>
                        {item.title}
                      </a>
                    </li>
                  )
                })}
              </ul>
              
            </>
          )
          :
          (
            <>
              <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                {MenuItemsGuest.map((item, index) => {
                  return (
                    <li key={index}>
                      <a className={item.cName} href={item.url}>
                        {item.title}
                      </a>
                    </li>
                  )
                })}
              </ul>
              <Button><a className="colorButton" href="/signup">Sign Up</a></Button>    
            </>
          )

        }
      </nav>
    )
  }
}

export default Navbar