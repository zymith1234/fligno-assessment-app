export const MenuItemsGuest = [
	{
		title: 'Home',
		url: '/',
		cName: 'nav-links'
	},
	{
		title: 'Login',
		url: '/login',
		cName: 'nav-links'
	},
]