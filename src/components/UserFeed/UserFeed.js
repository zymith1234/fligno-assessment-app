import React, {Component, useState} from 'react';
import Linkify from 'react-linkify';
import './UserFeed.css';
//import TimeAgo from 'react-timeago';
import {PostData} from '../../services/PostData';

class UserFeed extends Component {

	constructor(){
		super();
		this.state = {
			editedFeed: ""
		};
		this.editFeed = this.editFeed.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	editFeed(e){ 
		let updateIndex=e.target.getAttribute('value');
		
		let feedToUpdate = this.state.editedFeed
		console.log(feedToUpdate)
		let feed_id=document.getElementById("save").getAttribute("data");
		console.log(feed_id)
		let data = JSON.parse(sessionStorage.getItem("userData"));
		console.log(data)
		console.log(data.userData.user_id)
		let postData = { user_id: data.userData.user_id, feed_id: feed_id, feedToUpdate: feedToUpdate };
		if (postData) {
			PostData('feedEdit', postData).then((result) => {
				console.log(result)
			});
			window.location.reload(true);
		}		
	}

	onChange(e){
		this.setState({[e.target.name]:e.target.value});
	}


	render() {
		let userFeed = this.props.feedData
		.map(function (feedData, index) {		
				console.log(index)
				console.log(feedData)
				console.log(feedData.isEditing)
			return (
				
					feedData.isEditing == "0"
					? (
					<div className="medium-12 columns" key={index}>
						<div className="people-you-might-know">
							<div className="row add-people-section">
								<div className="small-12 medium-10 columns about-people">
									<div className="about-people-author">
										<p className="author-name">
											<b>{this.props.name}</b>
											<Linkify>{feedData.feed}</Linkify>
											<br/>
										</p>
									</div>
								</div>
									<div className="small-12 medium-2 columns add-friend">
										<div className="add-friend-action">
											<button id="edit" className="button small edit-btn-color" onClick={this.props.editMode} data={feedData.feed_id} value={this.props.index} >
												<i className="fa fa-user-times" aria-hidden="true"></i>
												Edit
											</button>
											<button id="del" className="button small btn-color" onClick={this.props.deleteFeed} data={feedData.feed_id} value={this.props.index} >
												<i className="fa fa-user-times" aria-hidden="true"></i>
												Delete
											</button>
										</div>
									</div>
								
							</div>
						</div>
					</div> )
					:
					(
					<div className="medium-12 columns" key={index}>
						<div className="people-you-might-know">
							<div className="row add-people-section">
								<div className="small-12 medium-10 columns about-people">
									<div className="about-people-author">
										<p className="author-name">
										
											<b>{this.props.name}</b>
											<input type="text" name="userFeed" onChange={this.onChange} name="editedFeed"  />
											<button id="save" className="button small edit-btn-color" onClick={this.editFeed} data={feedData.feed_id} value={this.props.index} type="submit">
												<i className="fa fa-user-times" aria-hidden="true"></i>
												Save
											</button>
											<button id="cancel" className="button small btn-color" onClick={this.props.cancelFeed} data={feedData.feed_id} value={this.props.index} >
												<i className="fa fa-user-times" aria-hidden="true"></i>
												Cancel
											</button>
											<br/>
										</p>
									</div>
								</div>
									<div className="small-12 medium-2 columns add-friend">
										<div className="add-friend-action">
											
										</div>
									</div>
								
							</div>
						</div>
					</div>
					)
				
				
			)	
		}, this);


	
		return (
			<div>
				{userFeed}

			</div>
		);
	}
}

export default UserFeed;