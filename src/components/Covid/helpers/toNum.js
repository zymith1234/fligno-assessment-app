
export default function toNum(casesStr){
	if (casesStr.length != null) {
		const arr = [...casesStr]
		const filteredArr = arr.filter(element => element !== ",")
		return parseInt(filteredArr.reduce((x,y)=> x + y))

	}
}