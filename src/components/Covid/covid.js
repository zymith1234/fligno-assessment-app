import React, {Component} from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron'
import toNum from './helpers/toNum'
import SearchPage from './CovidCountries/search'
import './covid.css';

export default function Covid() {
  return (
  	<>
	<Jumbotron className="text-center">
    	<h1> Covid-19 Tracker</h1>
    	<SearchPage />
    </Jumbotron>
 	</>
  )
}
