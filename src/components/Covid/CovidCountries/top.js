import React, {Component} from 'react';
import {useState, Fragment,useEffect,useRef} from 'react'
import toNum from '../helpers/toNum'
import Jumbotron from 'react-bootstrap/Jumbotron'
import DoughnutChart from '../CovidComponents/DoughnutChart'
import Banner from '../CovidComponents/Banner'
import {Row,Col,Container} from 'react-bootstrap'
import {Doughnut} from 'react-chartjs-2'

export default function Top10RecentCases(){

	const mapContainerRef = useRef(null)
	const [data, setData]=useState([])

	useEffect(()=>{
	fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",{
		method: "GET",
		headers: {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	}).then(res=>res.json())
	.then(result=>{
		let dataRaw = []
		dataRaw = result.countries_stat
		setData(dataRaw)
	})
	},[])


	console.log(data)

	useEffect(()=>{
		if (data.length != 0) {
		console.log(data[0].country_name)			
		}

	},[data])



	return (
		<Fragment>
		<Container>
			
			<h3 ><a href="/top10">Top 10 Covid-19 Countries</a></h3>
				
				<div>
				{
					data.length != 0
					?
					<div>
						<Banner country={data[0].country_name} deaths={data[0].deaths} recoveries={data[0].total_recovered} criticals={data[0].serious_critical} activeCases={data[0].active_cases} />
						<DoughnutChart criticals={data[0].serious_critical} deaths={data[0].deaths} recoveries={data[0].total_recovered} activeCases={data[0].active_cases} />
						
						<Banner country={data[1].country_name} deaths={data[1].deaths} recoveries={data[1].total_recovered} criticals={data[1].serious_critical} activeCases={data[1].active_cases} />
						<DoughnutChart criticals={data[1].serious_critical} deaths={data[1].deaths} recoveries={data[1].total_recovered} activeCases={data[1].active_cases} />
						
						<Banner country={data[2].country_name} deaths={data[2].deaths} recoveries={data[2].total_recovered} criticals={data[2].serious_critical} activeCases={data[2].active_cases} />
						<DoughnutChart criticals={data[2].serious_critical} deaths={data[2].deaths} recoveries={data[2].total_recovered} activeCases={data[2].active_cases} />

						<Banner country={data[3].country_name} deaths={data[3].deaths} recoveries={data[3].total_recovered} criticals={data[3].serious_critical} activeCases={data[3].active_cases} />
						<DoughnutChart criticals={data[3].serious_critical} deaths={data[3].deaths} recoveries={data[3].total_recovered} activeCases={data[3].active_cases} />

						<Banner country={data[4].country_name} deaths={data[4].deaths} recoveries={data[4].total_recovered} criticals={data[4].serious_critical} activeCases={data[4].active_cases} />
						<DoughnutChart criticals={data[4].serious_critical} deaths={data[4].deaths} recoveries={data[4].total_recovered} activeCases={data[4].active_cases} />

						<Banner country={data[5].country_name} deaths={data[5].deaths} recoveries={data[5].total_recovered} criticals={data[5].serious_critical} activeCases={data[5].active_cases} />
						<DoughnutChart criticals={data[5].serious_critical} deaths={data[5].deaths} recoveries={data[5].total_recovered} activeCases={data[5].active_cases} />

						<Banner country={data[6].country_name} deaths={data[6].deaths} recoveries={data[6].total_recovered} criticals={data[6].serious_critical} activeCases={data[6].active_cases} />
						<DoughnutChart criticals={data[6].serious_critical} deaths={data[6].deaths} recoveries={data[6].total_recovered} activeCases={data[6].active_cases} />

						<Banner country={data[7].country_name} deaths={data[7].deaths} recoveries={data[7].total_recovered} criticals={data[7].serious_critical} activeCases={data[7].active_cases} />
						<DoughnutChart criticals={data[7].serious_critical} deaths={data[7].deaths} recoveries={data[7].total_recovered} activeCases={data[7].active_cases} />

						<Banner country={data[8].country_name} deaths={data[8].deaths} recoveries={data[8].total_recovered} criticals={data[8].serious_critical} activeCases={data[8].active_cases} />
						<DoughnutChart criticals={data[8].serious_critical} deaths={data[8].deaths} recoveries={data[8].total_recovered} activeCases={data[8].active_cases} />

						<Banner country={data[9].country_name} deaths={data[9].deaths} recoveries={data[9].total_recovered} criticals={data[9].serious_critical} activeCases={data[9].active_cases} />
						<DoughnutChart criticals={data[9].serious_critical} deaths={data[9].deaths} recoveries={data[9].total_recovered} activeCases={data[9].active_cases} />

					</div>
					:
					<></>
				}
				</div>
		</Container>
		</Fragment>


		)


}

