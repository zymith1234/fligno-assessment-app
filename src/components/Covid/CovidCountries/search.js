import React, {Component} from 'react';
import {useState, Fragment, useRef, useEffect} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import toNum from '../helpers/toNum'
import Jumbotron from 'react-bootstrap/Jumbotron'
import DoughnutChart from '../CovidComponents/DoughnutChart'
import Banner from '../CovidComponents/Banner'
import {Row,Col,Container} from 'react-bootstrap'


export default function SearchPage(){
	const [data,setData] = useState([])
	const countriesStats = data.countries_stat
	const [targetCountry, setTargetCountry] = useState('')
	const [name, setName] = useState('')
	const [criticals ,setCriticals] = useState(0)
	const [deaths, setDeaths] = useState(0)
	const [recoveries,setRecoveries] = useState(0)
	const [activeCases,setActiveCases] = useState(0)
	const [latitude,setLatitude] = useState(0) 
	const [longitude,setLongitude] = useState(0)
	const [zoom, setZoom] = useState(0)
	
	useEffect(()=>{
	fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",{
		method: "GET",
		headers: {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	}).then(res=>res.json())
	.then(result=>{
		let dataRaw = []
		dataRaw = result.countries_stat
		setData(dataRaw)
	})
	},[])

	useEffect(()=>{
		console.log(data)
	},[data])

	const mapContainerRef = useRef(null)

	function search(e){
		e.preventDefault()
		
		const match = data.find(country => {
			
			return country.country_name.toLowerCase() === targetCountry.toLowerCase()
		})
		console.log(match)
		if(match){
			setName(match.country_name)
			setCriticals(toNum(match.serious_critical))
			setDeaths(toNum(match.deaths))
			setRecoveries(toNum(match.total_recovered))
			setActiveCases(toNum(match.active_cases))
			
		}
	}
	
	return(
		<Fragment>
		<Container>
			<h3><a href="/top10">Top 10 Covid-19 Countries</a></h3>
			<h5>Search Country</h5>
			<Form onSubmit={e => search(e)}>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control type="text" placeholder="Search for country" value={targetCountry} onChange={e => setTargetCountry(e.target.value)}/>
					<Form.Text className="text-muted">
						Get Covid-19 Statistics for Searched Country.
					</Form.Text>
				</Form.Group>
				<Button variant="primary" type="submit">Submit</Button>
			</Form>
				<div>
					<Banner country={name} deaths={deaths} recoveries={recoveries} criticals={criticals} activeCases={activeCases} />
					<DoughnutChart criticals={criticals} deaths={deaths} recoveries={recoveries} activeCases={activeCases}/>
					
				</div>
		</Container>
		</Fragment>
	)
}


