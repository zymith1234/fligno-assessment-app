import React, {Component} from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'

export default function Banner({country, deaths, recoveries, criticals, activeCases}){
	
	return(
		<div>
		
		<h4 className="text-center">{country}</h4>
		<p className="text-center">Deaths: {deaths}</p>
		<p className="text-center">Recoveries: {recoveries}</p>
		<p className="text-center">Critical Cases: {criticals}</p>
		<p className="text-center">Active Cases: {activeCases}</p>
	
		</div>
	)
}