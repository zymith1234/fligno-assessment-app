import React, {Component} from 'react';
import {Doughnut} from 'react-chartjs-2'
import toNum from '../helpers/toNum'
import {useState, useEffect} from 'react'

export default function DoughnutChart({activeCases,criticals,deaths,recoveries}){
	
	const [activeCasesInt, setActiveCasesInt] = useState(0)	
	const [criticalsInt, setCriticalInt] = useState(0)
	const [deathsInt, setDeathsInt] = useState(0)
	const [recoveriesInt, setRecoveriesInt] = useState(0)
	

	useState(()=>{
		setActiveCasesInt(toNum(activeCases))
		setCriticalInt(toNum(criticals))
		setDeathsInt(toNum(deaths))
		setRecoveriesInt(toNum(recoveries))
	},[activeCases])

	return(
		<Doughnut data={{
			datasets: [{
				data: [activeCasesInt,criticalsInt,deathsInt,recoveriesInt], //array of numbers and data only accepts numbers
				backgroundColor: ["indianRed", "red", "black", "green"]
			}],
			labels: [
				'Active Cases',
				'Criticals',
				'Deaths',
				'Recoveries'
			]
		}}/>
	)
}