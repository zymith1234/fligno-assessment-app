import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Welcome from '././components/Welcome/Welcome';
import Home from '././components/Home/Home';
import Login from '././components/Login/Login';
import Signup from '././components/Signup/Signup';
import NotFound from '././components/NotFound/NotFound';
import Covid from '././components/Covid/covid';
import Top10RecentCases from '././components/Covid/CovidCountries/top';

const Routes = () => (
	<BrowserRouter >
		<Switch>
			<Route exact path="/" component={Welcome}/>
			<Route path="/home" component={Home}/>
			<Route path="/login" component={Login}/>
			<Route path="/Signup" component={Signup}/>
			<Route path="/covid" component={Covid}/>
			<Route path="/top10" component={Top10RecentCases}/>
			<Route path="*" component={NotFound}/>
		</Switch>
	</BrowserRouter>
);

export default Routes;